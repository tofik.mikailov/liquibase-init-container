package az.ingress.sampleproject.service.impl;

import az.ingress.sampleproject.dto.EmployeeRequestDto;
import az.ingress.sampleproject.dto.EmployeeResponseDto;
import az.ingress.sampleproject.mapper.EmployeeMapper;
import az.ingress.sampleproject.model.Employee;
import az.ingress.sampleproject.repository.EmployeeRepository;
import az.ingress.sampleproject.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    @Override
    public EmployeeResponseDto createEmployee(EmployeeRequestDto employeeDto) {
        Employee employee = employeeRepository.save(employeeMapper.dtoToEmployee(employeeDto));
        return employeeMapper.employeeToDto(employee);
    }

    @Override
    public EmployeeResponseDto getEmployeeById(Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Employee with id %s not found", id)));
        return employeeMapper.employeeToDto(employee);
    }
}
