package az.ingress.sampleproject.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeResponseDto implements Serializable {

    private Long id;
    private String name;
    private String surname;
    private BigDecimal salary;
}
