package az.ingress.sampleproject.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeRequestDto implements Serializable {

    @Length(min = 3, max = 50, message = "Name '${validatedValue}' must be between {min} and {max} length")
    @NotNull(message = "Name can not be null")
    private String name;
    @Length(min = 3, max = 50, message = "Surname '${validatedValue}' must be between {min} and {max} length")
    @NotNull(message = "Surname can not be null")
    private String surname;
    @NotNull(message = "Salary can not be null")
    @Positive(message = "Salary can be only positive")
    private BigDecimal salary;
}
