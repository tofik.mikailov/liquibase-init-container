package az.ingress.sampleproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"az.ingress.sampleproject.*"})
public class SampleprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleprojectApplication.class, args);
    }

}
