package az.ingress.sampleproject.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import az.ingress.sampleproject.dto.EmployeeRequestDto;
import az.ingress.sampleproject.dto.EmployeeResponseDto;
import az.ingress.sampleproject.model.Employee;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface EmployeeMapper {

    Employee dtoToEmployee(EmployeeRequestDto dto);

    EmployeeResponseDto employeeToDto(Employee employee);

}
