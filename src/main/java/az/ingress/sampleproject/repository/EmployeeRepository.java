package az.ingress.sampleproject.repository;

import az.ingress.sampleproject.model.Employee;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeRepository
        extends PagingAndSortingRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

    @Override
    Optional<Employee> findById(Long id);
}
