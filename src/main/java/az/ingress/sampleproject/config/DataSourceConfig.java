package az.ingress.sampleproject.config;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {

    @Value("${spring.profiles.active}")
    private String profile;

    @Value("${spring.liquibase.enabled}")
    private boolean liquibaseEnabled;

    private SpringLiquibase springLiquibase(DataSource dataSource, LiquibaseProperties properties) {
        properties.setChangeLog(String.format("classpath:db/changelog/db.changelog-%s.yaml", profile));
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(properties.getChangeLog());
        liquibase.setContexts(properties.getContexts());
        liquibase.setDefaultSchema(properties.getDefaultSchema());
        liquibase.setDropFirst(properties.isDropFirst());
        liquibase.setShouldRun(properties.isEnabled());
        liquibase.setLabels(properties.getLabels());
        liquibase.setChangeLogParameters(properties.getParameters());
        liquibase.setRollbackFile(properties.getRollbackFile());
        return liquibase;
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.liquibase")
    public LiquibaseProperties primaryLiquibaseProperties() {
        return new LiquibaseProperties();
    }

    @Bean
    public SpringLiquibase primarySpringLiquibase(DataSource dataSource) {
        SpringLiquibase liquibase = springLiquibase(dataSource, primaryLiquibaseProperties());
        liquibase.setShouldRun(liquibaseEnabled);
        return liquibase;
    }
}
